% ----------------------------------------------------------------------------
% Silba y acudiré
% Montague Rhodes James
% Traducción de Ariel Sebastián Becker (Arseb).
% © 2016. Todos los derechos reservados.
% Texto original en http://www.thin-ghost.org/items/show/150
% ----------------------------------------------------------------------------
\documentclass[8pt]{book}
\usepackage{bu-paper-a4}
\usepackage{comment}
\usepackage{pdfpages}
\usepackage{hyperref}
\begin{document}
\frontmatter{}
	\input{inclusiones/caratula}
\mainmatter{}
\tableofcontents{}
\setlength{\parskip}{8pt}
\chapter*{Sumario}

La primera criptodivisa en alcanzar una adopción masiva fue Bitcoin, que presentó al mundo un uso real para los conceptos de \textit{moneda descentralizada} y \textit{blockchain}; luego, con la llegada de Ethereum, se introdujo la noción de \textit{contratos inteligentes}. Si bien la idea del blockchain han logrado un desarrollo y prosperidad significativos en los últimos tres años, su tecnología dista mucho de ser perfecta, y comienza a enfrentar desafíos significativos en su integración con los requerimientos actuales del mercado.

Este libro blanco se centra principalmente en tres conceptos: el rango de valor de los \textit{blockchain}, el ecosistema de retroalimentación positiva y el sistema \textit{blockchains} auto-evolutivos. En base a esto, presentamos la idea y las soluciones de Nebulas, que consisten en \textbf{Nebulas Rank} (una sistema de medición del valor de los blockchains) y \textbf{Nebulas Force} (un mecanismo fundamental de auto-evolución de los mismos). También desarrollaremos un mecanismo para generar innovación mediante consenso: \textbf{PoD} (\textit{Proof of Devotion}, o Prueba de Devoción) y \textbf{DIP} (\textit{Developer Incentive Protocol}, o Protocolo de Incentivos para Desarrolladores).

La misión última de nuestro proyecto es la de construir un sistema de blockchains auto-evolutivos, basado en incentivos de valor, que también será la directriz de desarrollo más importante en el mundo de los blockchains de aquí en adelante. Como es norma en la industria actual, los componentes principales —tales como contratos inteligentes, sistemas de nombres de dominio, juegos de herramientas para desarrolladores, carteras, etcétera— están previstos para el lanzamiento de la plataforma Nebulas.

\chapter{Antecedentes}

El 31 de octubre de 2008 Satoshi Nakamoto publicó un libro blanco titulado: «\href{https://bitcoin.org/bitcoin.pdf}{Bitcoin: A Peer-to-Peer Electronic Cash System}», que marcó el nacimiento del concepto de \textit{blockchain} en nuestro mundo.

Como aplicación primordial del blockchain, Bitcoin pone en práctica su ideal de \textit{sistema de moneda digital descentralizada}, e introduce al mismo tiempo el gigantesco valor y potencial de los blockchains mundiales. Poco a poco, más y más gente comienza a seguir, estudiar y dedicar tiempo y esfuerzo a comprender los blockchains, lo que promueve el desarrollo sustentable de su infraestructura. Así, la \textit{comunidad del blockchain} contribuye a crear una mayor conexión entre nuestro mundo real y sus datos, abriendo así nuevas fronteras permanentemente.

Cabe señalar que la tecnología del blockchain no es en completamente nueva, sino más bien un nuevo modelo de colaboración basado en una combinación de tecnologías que incluyen redes p2p, criptografía, y estructuras de datos, entre otras. Un blockchain es esencialmente, entonces, un sistema autónomo y descentralizado, con base en la \textit{\href{https://es.wikipedia.org/wiki/Teoría_de_juegos}{teoría del juego}}, cuyo verdadero interés está en su \textbf{modelo de colaboración abierta}, basado en el mecanismo de consenso y bajo la idea de la descentralización.

Estamos satisfechos al ver que la función de los blockchains está abandonando su original monotonía para abrazar la diversidad, mientras todavía se encuentra en etapa experimental y de grandes avances.

Mientras Bitcoin es un sistema de caja descentralizado, los blockchains de segunda generación —de los que Ethereum es pionero— plantean el novedoso concepto de \textit{contrato inteligente} (sistema Turing-completo), que permite tener blockchains inteligentes; esto es, con capacidad de tomar decisiones y de realizar procesos lógicos. En paralelo, otros blockchains proponen conceptos más amplios como \textit{interoperabilidad entre cadenas}, \textit{sistema operativo en blockchain}, etcétera. Todos estos esfuerzos llevan a que la tecnología blockchain trascienda a un espacio de mayor dimensionalidad.

\chapter{Oportunidades y desafíos}

Creemos que los sistemas descentralizados y autónomos (representados actualmente por la tecnología blockchain), son una clara tendencia en la actualidad, que atrae cada vez más la atención de la sociedad. Mientras tanto, varios proyectos blockchain se están desarrollando a gran velocidad tremenda, y el valor de los activos criptográficos está aumentando a un ritmo exponencial.

Según estimaciones, se espera que el mercado mundial de tecnología blockchain ascienda a USD 20 000 000 000 a finales de 2024, en comparación con los USD 315 900 000 de 2015. A finales de 2017, el límite global del mercado de activos criptográficos ha alcanzado los USD 610 000 000 000, frente a los USD 18 000 000 000 de 2016. Al mismo tiempo, la población de usuarios de blockchain y activos digitales está aumentando de forma vertiginosa. A principios de 2013, los usuarios globales de la industria eran 2 millones, y a principios de 2018 ese número había crecido a 20 millones. Creemos que \textbf{alrededor de 2020, los usuarios globales de blockchains y activos digitales alcanzarán —o superarán— los 200 millones, y alrededor del año 2025, se espera que esta cifra alcance los 1000 millones.} Sobre la base de esta estimación, la innovación en los blockchains se encuentra actualmente frente a una ventana de oportunidades doradas. Al mismo tiempo los blockchains también se enfrentan a muchos desafíos, como la interacción de la información sobre blockchains, la actualización de protocolos, las aplicaciones blockchain, la eficacia y la relevancia de los contratos inteligentes, etc.

\textbf{El problema de la interacción de la información en los blockchains} tiene su causa en la falta de interoperabilidad entre sus diferentes redes. Se ha llegado a un consenso en la industria sobre este problema, pero hasta ahora no se ha logrado una solución perfecta. La cuestión crucial en este caso se basa en las interacciones de los datos o activos, tanto en el blockchain como fuera del mismo, así como en las cadenas cruzadas . Esto ha restringido significativamente todo el potencial de la tecnología de los blockchains, lo que ha provocado los llamados \textit{silos de información} en la cadena y fuera de ella. Cada vez más proyectos blockchain están tratando de lograr avances en este tema, pero, por ahora, no existen protocolos y estándares unificados para la interacción y el manejo de datos y activos en diferentes blockchains, lo que genera una deficiencia de interoperabilidad, tanto interna como externa, entre ellos.

A diferencia de las actualizaciones tradicionales de software, \textbf{actualizar un protocolo de red de blockchain} a menudo desencadena \textit{forks} (bifurcaciones) de tipo \textit{blandas} o \textit{duras}. Un \textit{hard fork} (o bifurcación dura) sucede cuando un activo digital almacenado en un blockchain (una criptodivisa, por ejemplo) se divide en dos entidades distintas, cada uno con un nuevo blockchain. Esta situación ocurre cuando se cambia el código existente de una criptodivisa, lo que resulta en una versión antigua y una nueva. Por otro lado, un \textit{soft fork} (bifurcación blanda) es en esencia igual a la primera, aunque en este caso sólo una de los dos blockchains será válido, y, por ende, sólo una de las dos criptodivisas fruto del \textit{fork} tendrá validez; esta validez se determina mediante el nivel de adopción de ambas: el \textit{fork} con mayor cantidad de adherentes será el que mantenga su vigencia.

Bitcoin, por ejemplo, experimentó un \textit{hard fork} en agosto de 2017, debido a un problema de escalabilidad. Ese problema surgió del hecho de que los bloques en el blockchain estaban limitados a un espacio de 1 MB. Se estima que las recompensas por minado llegaron a la suma de USD 25 por transacción a fines de 2017, lo que hizo que las transacciones por valores bajos no fueran rentables.

En contraste con el pico de 24 000 transacciones por segundo de la red VISA, la capacidad máxima teórica de la red bitcoin —con el límite de 1 MB por bloque— se sitúa entre 3,3 y 7 transacciones por segundo. Debido a ello, se produjo un incidente en el que casi 1 millón de transacciones quedaron pendientes en cola. En las circunstancias actuales, existe una gran incertidumbre sobre el tiempo de transferencia de Bitcoin, y los usuarios tienen que pagar una tarifa extra en concepto de \textit{aceleración de transacciones} que es muy elevada, lo que perjudica la experiencia del usuario.

Ethereum también experimentó \textit{hard forks}. En 2016, como resultado del colapso del proyecto DAO, Ethereum se dividió en dos blockchains separados: la nueva versión se convirtió en Ethereum (ETH), y la original continuó como Ethereum Classic (ETC). A pesar de resolver el problema temporalmente, el \textit{fork} todavía producía \textit{activos duplicados} en ambos blockchains, así como la división de la comunidad como un efecto colateral.

Con el rápido crecimiento de las aplicaciones distribuidas (DApps), \textbf{un motor de búsqueda eficiente para aplicaciones y contratos inteligentes} se ha convertido en uno de los mayores retos para la tecnología blockchain. Existen cientos de aplicaciones descentralizadas basadas en Ethereum, y su número se incrementa día a día; esto plantea un problema de escalabilidad, en el sentido de que será un gran problema buscar y encontrar las DApps adecuadas para cada necesidad. Desde este punto de vista, \textbf{carecemos de una medida de más dimensiones de información y valor de rango, lo que impide el descubrimiento de las maravillas que existen en los distintos blockchains disponibles para los usuarios.}

\chapter{¿Por qué elegir Nebulas?}

\begin{center}
	\includegraphics[width=0.9\textwidth]{ilustraciones/1.png}\par\vspace{1cm}
\end{center}

Al enfrentarnos a los desafíos descritos en la sección anterior, apuntamos a crear un sistema blockchain auto-evolutivo basado en \textit{incentivos de valor}.

\begin{center}
	\includegraphics[width=0.9\textwidth]{ilustraciones/2.png}\par\vspace{1cm}
\end{center}

Específicamente, nuestros objetivos son:

\section{Valuación de los datos}

Creemos que hace falta un estándar para la medición del valor de los datos subyacentes en los blockchains, de modo de construir y explorar información en un orden dimensional mayor.

\section{Crear feedback positivo para el ecosistema comunitario}

Para construir un ecosistema de aplicaciones próspero y descentralizado, es fundamental contar con un mecanismo de retroalimentación positiva para el desarrollador. Sólo de esta forma es posible potenciar el sistema de blockchain con una mayor vitalidad.

\section{Alcanzar la evolución autónoma}

Todo blockchain dotado de un mecanismo de auto-evolución, y con poca intervención externa, será capaz de  proporcionar mayor velocidad de procesamiento, un sistema más potente y una mejor experiencia de usuario.

\chapter{Características técnicas}

\section{Nebulas Rank (NR)}

\textbf{Nebulas Rank} (NR) es el algoritmo de medición y valuación (\textit{ranking}) de Nebulas, y es la unidad de medida del valor de los datos en el mundo blockchain. Sus mediciones se basan en la liquidez de los activos digitales, la interactividad entre usuarios y la propagación de activos entre ellos. NR se utiliza para clasificar y valuar direcciones, contratos inteligentes, aplicaciones distribuidas (DApps) y otras entidades en la cadena de bloques, y es de código abierto.

\begin{center}
	\includegraphics[width=0.9\textwidth]{ilustraciones/3.png}\par\vspace{1cm}
\end{center}

Como hemos dicho, la valuación se obtiene mediante tres parámetros:

\subsection{Liquidez}

Las finanzas son esencialmente la actividad social que optimiza los recursos sociales a través de la liquidez del capital y promueve el desarrollo económico. Los blockchains establecen una red de valor en la que pueden fluir esos activos financieros. El volumen diario combinado de Bitcoin y Ethereum —que nos son más familiares— ya supera los USD 1000M. A partir de estos datos, podemos ver que cuanto mayor es el volumen y la escala de las transacciones, mayor es la liquidez. A su vez, una mayor liquidez aumenta la cantidad de transacciones y aumenta, por consiguente, su valor. Esto fortalece aun más el valor de los activos financieros, creando un mecanismo completo de retroalimentación positiva. \textbf{Por lo tanto, la liquidez, es decir, la frecuencia y la escala de las operaciones, es la primera dimensión que mide NR}.

\subsection{Propagación}

WeChat y Facebook tienen casi 3000 millones de usuarios activos al mes. El rápido crecimiento de los usuarios de las plataformas sociales es el resultado del reflejo de las redes sociales existentes y de un mayor crecimiento viral. En particular, la transmisión viral —esto es, la velocidad, el alcance, la profundidad de la transmisión y la vinculación de la información— es un indicador clave para supervisar la calidad y el crecimiento de las redes sociales por parte de los usuarios. En el mundo del blockchain podemos ver el mismo patrón. La propagación viral indica el alcance y la profundidad de la liquidez de los activos, lo que hace posible el incremento en la calidad y escala de los activos digitales almacenados en blockchains. En pocas palabras, \textbf{la transmisión viral, que indica el alcance y la profundidad de la liquidez de los activos, es la segunda dimensión que mide NR}.

\subsection{Interoperabilidad}

Durante los primeros años de Internet sólo existían sitios web básicos e información privada. Ahora, la información disponible en las diferentes plataformas se puede copiar o compartir en cualquier otra red, lo que provoca la ruptura de los pocos silos de datos que quedan en pie. Esta tendencia marca el proceso de identificación de información en un orden dimensional mayor.

Desde nuestro punto de vista, el mundo del blockchain seguirá un patrón similar, aunque a mayor velocidad. La información sobre usuarios, activos digitales, contratos inteligentes y DApps se enriquecerá, y la interacción de la información en un orden dimensional mayor será más frecuente. \textbf{Esto requiere una mejor interoperabilidad, lo que, justamente, es el tercer parámetro de NR}.

Basándonos en los parámetros anteriores comenzamos a construir el sistema NR de Nebulas, a partir de datos más ricos, construyendo un modelo mejor, buscando dimensiones de valor más diversificadas y estableciendo la valuación del mundo de los blockchains.

\section{Proof-of-Devotion (PoD)}

El algoritmo de consenso PoD (Proof-of-Devotion) se basa en el sistema de valuación Nebulas Rank. PoD le da a un usuario \textit{influyente} dentro del blockchain de Nebulas la oportunidad de convertirse en un \textit{contador} (rol de \textit{bookkeeper}) y recibir recompensas por bloques y transacciones procesados, lo cual les animará a contribuir de forma continua tanto a la estabilidad como a la seguridad del blockchain de Nebulas.

\subsection{Concepto central}

Los usuarios cuya valuación NR sea mayor que un umbral determinado podrán formar parte del proceso de selección de \textit{contadores} (\textit{bookkeepers}) mediante el pago de un depósito de caución; así, a través de un proceso de \textit{minado}, cada candidato a \textit{contador} competirá para obtener \textit{derechos de contaduría}; los usuarios con estos derechos serán responsables de la generación de nuevos bloques, y a cambio de ello recibirán dos recompensas: una por cada bloque minado, y otra mediante las comisiones por transacción; en caso de que el comportamiento de un usuario generara problemas, su depósito de caución será confiscado y reasignado a otros candidatos a contador.

\section{Developer Incentive Protocol (Protocolo de Incentivo para Desarrolladores)}

Este concepto se creó especialmente para desarrolladores de contratos inteligentes y aplicaciones descentralizadas.

\subsection{Concepto central}

El mecanismo es sencillo: luego de que se generan una cierta cantidad de nuevos bloques en el blockchain de Nebulas, se procede a otorgar recompensas a todos aquellos desarrolladores cuyos contratos inteligentes y aplicaciones descentralizadas se implementan con un valor \textit{Nebulas Rank} superior a un umbral previamente especificado. Esto permite, por un lado, recompensar por su esfuerzo a los desarrolladores que proveen valor agregado al ecosistema de Nebulas; por otro lado, incentiva la creación de nuevas DApps y contratos inteligentes de calidad.

\section{Nebulas Force (NF)}

Vimos con anterioridad la introducción a los tres protocolos fundamentales de Nebulas: NR, PoD y DIP. Estos formarán parte de los datos del blockchain de Nebulas; al crecer los datos, los protocolos asociados se irán actualizando. Esta habilidad fundamental de nuestro blockchain se llama \textit{Nebulas Force} (NF).

Esta característica proporciona al blockchain de Nebulas (y a las aplicaciones distribuidas construidas sobre ella) la capacidad de auto-evolucionar. Con NF los desarrolladores serán capaces de realizar cambios, incorporar nuevas tecnologías y corregir errores sin necesidad de recurrir a un \textit{hard fork}.

Nebulas Force —y su capacidad de actualizar los protocolos— se abrirá a la comunidad a medida que esta crezca. De acuerdo al peso de la \textit{valuación NR} de los usuarios, y utilizando el mecanismo de votación de la comunidad, ésta podrá determinar la evolución y dirección del blockchain de Nebulas y sus objetivos de actualización. Con la ayuda de la tecnología central de NF (y su apertura a la comunidad), Nebulas tendrá un potencial siempre creciente e infinitamente evolutivo.

En paralelo a esto, Nebulas está desarrollando un mecanismo a futuro para la Evolución Polimórfica del Cluster de Nebulas (\textit{Nebulas Cluster Polymorphous Evolution}). Partiendo de la premisa de que los datos y activos digitales del blockchain son seguros, se crearán uno o varios blockchain laterales a través del \textit{protocolo innato de excitación} de Nebulas (\textit{Nebulas Excitation Protocol}) con el fin de verificar las soluciones básicas de innovación técnica, tales como el nuevo algoritmo de consenso, la red Lightning, etc. Al mismo tiempo, por medio \textit{Nebulas Wormhole}, los datos y activos digitales podrán ser intercambiados entre blockchains laterales o entre ellos y el blockchain principal, y se garantizará la interoperabilidad entre aplicaciones descentralizadas.

Durante este proceso los usuarios del blockchain principal de Nebulas podrán invertir para recibir NAS acuñados mediante la nueva tecnología disponible en los blockchains laterales. También tendrán la opción de mantener sus activos en el blockchain principal y aun así beneficiarse de los avances surgidos de la evolución de esos blockchains laterales; debido a que la optimización de la performance incrementa los ingresos de los usuarios, Nebulas puede actualizar su arquitectura central de una forma segura y económica, brindando así a su ecosistema una vitalidad y escalabilidad sin precedentes.

Cuando las innovaciones técnicas de los blockchains laterales no sean aceptadas por el blockchain principal, es posible dejar que continúen desarrollándose. Mediante el Wormhole de Nebulas, la actividad en los blockchains laterales podrán hacer uso de los intercambios de datos y activos entre sí, como así también brindar interoperabilidad a las aplicaciones descentralizadas. Si los sistemas blockchain como Bitcoin, Litecoin, Ethereum y Ethereum Classic tuvieran una capacidad nativa de intercambio de datos y activos entre ellos, nos enfrentaríamos inmediatamente a muchas más dimensiones de posibilidades técnicas.

Véase la sección 8 más abajo para obtener más información acerca de potenciales distribuciones futuras de NAS.

\begin{center}
	\includegraphics[width=0.9\textwidth]{ilustraciones/4.png}\par\vspace{1cm}
\end{center}

\chapter{Ejemplos de uso}

Desde la creación del bloque inicial de Bitcoin en 2009, la comunidad ha llegado a un consenso sobre el objetivo de la mayoría de los blockchains públicos existentes: crear una infraestructura para construir un ecosistema DApp que sea respetuoso con los desarrolladores y que ofrezca un mecanismo completo de incentivos. Sin embargo, debido a diversas restricciones históricas, hasta la fecha todavía no existe un mecanismo tan sistemático y eficiente. De esa forma, desde el principio, Nebulas está haciendo el esfuerzo de construir un ecosistema de retroalimentación positiva abierto y transparente para la comunidad de desarrolladores.

En comparación con la industria actual de Internet, cualquier plataforma capaz de atraer un grupo diverso de desarrolladores es la más valiosa. Por citar un ejemplo, el \textit{Apple App Store} abrió perspectivas completamente nuevas para las aplicaciones móviles al establecer un ecosistema de aplicaciones que crea, además de beneficios empresariales, un enorme valor social. Desafortunadamente, incluso en ese caso todavía existen muchos defectos en el modelo de dividendos para quienes desarrollan aplicaciones para la plataforma, ya que no pueden compartir el bono de la plataforma debido al creciente número de aplicaciones.

¿Es posible establecer un mecanismo de incentivos para desarrolladores más inclusivo, garantizado por la tecnología de blockchains? En un mundo así, cada desarrollador de DApp tendría la oportunidad de compartir los beneficios del ecosistema basándose en sus contribuciones al ecosistema. En el blockchain de Nebulas esto es posible a través del protocolo DIP. Aquellos desarrolladores que hagan contribuciones al crecimiento del ecosistema comunitario serán recompensados con NAS, lo que creará un círculo virtuoso.

A través de NR, PoD y DIP, la cadena de bloques de Nebulas podría poner fin al actual modelo de incubación de capital de riesgo que es comparativamente centralizado, ineficiente, opaco y poco amigable, creando en su lugar una forma de motivar la creatividad y la vitalidad de los desarrolladores. Imaginemos, por ejemplo, qué sucedería si cada equipo que crea aplicaciones para el Apple App Store recibiera de ellos incentivos considerables, basados en sus contribuciones. En este paradigma, el ecosistema de aplicaciones probablemente entraría en una nueva era con una explosión de calidad y cantidad. La visión de Nebulas es la de institucionalizar un ecosistema descentralizado para la incubación de aplicaciones de desarrolladores a través de los incentivos DIP; creemos que el mundo descentralizado e incentivado creado por el blockchain de Nebulas resultará en la aparición de un número significativo de nuevas aplicaciones descentralizadas.

Cada día vemos más y más desarrolladores de aplicaciones descentralizadas unirse a la comunidad del blockchain para volcar a él más y más información. A medida que esto sucede, se hace más importante que la presentación de esa información a los usuarios se realice de forma sencilla y eficiente.

Esperamos ampliar aún más la funcionalidad de NR, por ejemplo, creando índices de información para blockchains. Al combinar NR con un mecanismo de palabras clave, podemos clasificar y gestionar la información de forma relevante. De este modo, los usuarios de blockchains podrán acceder a datos más relevantes y de mayor valor (incluyendo información, activos digitales, contratos inteligentes, aplicaciones descentralizadas, etcétera). En comparación con el motor de búsqueda de Google, con el explosivo y enorme crecimiento de la información en los blockchains, un índice de información y un motor de búsqueda para ese sistema se convierte en un requisito indispensable y necesario. Con el poder del índice y la búsqueda de información, además de combinar el modelo de negocio maduro de Internet, Nebulas espera abrir un potencial de negocio sin precedentes.

\chapter{Equipo}
\section{Fundadores}

\section{Hitters Xu}

Fundador de Nebulas \& Antshares (NEO), antiguo líder fundador de la Plataforma Blockchain de Ant Financial, ex-Googler en el equipo de Búsqueda y Lucha contra el Fraude, y graduado de la Universidad de Tongji en Ciencia y Tecnología Informática. Primer pionero del blockchain en China. Desde 2013, ha fundado BitsClub (la primera comunidad de innovación Blockchain-Bitcoin en China), Antshares (el primer blockchain en China), FBG (el primer fondo dedicado a blockchain en China), Gempay (la primera plataforma de pagos transfronterizos Bitcoin-Blockchain en China). En 2014 fundó la primera cumbre internacional de blockchains en China.

\section{Robin Zhong}

Cofundador de Nebulas, ex arquitecto de la plataforma Blockchain de Ant Financial, ex director senior de desarrollo de Dolphin Browser y líder de la división de juegos. Graduado de la Universidad de Ciencia y Tecnología de Huazhong. También es el fundador de Tongxinclub, la primera plataforma de apoyo mutuo Blockchain en China.

\section{Guan Wang}

Cofundador de Nebulas \& Antshares (NEO), fundador de OpenIP \& IP Community; un emprendedor en serie en la industria del blockchain. Graduado de la Universidad Southeast.

\chapter{Hoja de ruta}

\begin{center}
	\includegraphics[width=0.9\textwidth]{ilustraciones/5.png}\par\vspace{1cm}
\end{center}

\chapter{Sumario de asignación y distribución de NAS}

Inicialmente existirá un suministro de 100 000 000 de NAS que estarán destinados a ser utilizados entre los usuarios dentro de la plataforma Nebulas. Debido a que no se espera que los NAS sean \textit{quemados} y/o invalidados en el blockchain, operarán principalmente como (i) \textit{moneda reutilizable}; y (ii) como un instrumento para operar en la plataforma Nebulas. En el futuro, Nebulas podrá crear NAS adicionales en relación con las operaciones y funcionalidad de la plataforma, tal y como se indica a continuación.

La intención es crear una economía interna dentro de la plataforma Nebulas, por lo que la criptodivisa NAS será una parte integral del ecosistema. Por lo tanto, la criptodivisa NAS circulará continuamente dentro del ecosistema de Nebulas y de su plataforma, pasando de un usuario a otro como un medio para el desempeño de los roles vitales para el funcionamiento de la misma.

Nebulas incluirá en su núcleo el llamado \textit{Nebulas Rank} o \textit{NR}, un algoritmo de clasificación utilizado para valuar los datos y aplicaciones en su blockchain. NR se utilizará para clasificar direcciones, contratos inteligentes, aplicaciones distribuidas y otras entidades dentro de su blockchain. Basado en el sistema NR, como también se discutió, Nebulas adoptará un algoritmo de consenso llamado Proof of Devotion (\textit{Prueba de Devoción}) o \textit{PoD}, que les ofrecerá a los usuarios influyentes de la plataforma la oportunidad de convertirse en \textit{contables} y recibir recompensas por bloques creados y por las comisiones de cada transacción como ingresos, lo que les animará a contribuir a la estabilidad y seguridad de la cadena de bloques de Nebulas de forma continua.

Con el fin de apoyar el proceso de consenso del PoD, Nebulas se reservará el derecho de acuñar ocasionalmente NAS adicionales. Se estima que a fines de 2019 Nebulas generará NAS a una tasa anual del 3\% de la oferta total en relación con el programa PoD.

Finalmente, Nebulas también creará el llamado \textit{Developer Incentive Protocol} (Protocolo de Incentivos para Desarrolladores) o \textit{DIP}, un programa de incentivos para desarrolladores de contratos inteligentes y aplicaciones descentralizadas en la plataforma Nebulas. Con el fin de estimular este programa, Nebulas se también reserva el derecho de acuñar NAS adicionales, del mismo modo que en el punto anterior.

Sin perjuicio de lo anterior, Nebulas no asegura que en un futuro se generen NAS adicionales. Además, los actuales titulares de NAS tendrán derecho a determinar anualmente si los incentivos para los programas de PoD y DIP deben ser superiores o inferiores al 3\% de la oferta total de NAS. Tal determinación podría impactar el número de NAS, si los hubiera, creados por Nebulas en el futuro.

La distribución de los NAS, programada para principios de 2018, está prevista en la actualidad de la siguiente manera:

![NAS Distribution Plan](6.png)

\section{Oferta a la comunidad}

De acuerdo al plan de desarrollo del proyecto y sus requerimientos, la distribución de NAS estará a cargo del equipo fundador de Nebulas, y se hará en distintas etapas.

\section{Incentivos para los fundadores y el equipo de desarrollo}

Desde la fundación de Nebulas, tanto sus fundadores como su equipo técnico aportan continuamente recursos humanos y materiales para el desarrollo y la operación del ecosistema. Por ello, el 20\% del total de NAS emitidos quedan reservados para ellos a modo de incentivo. Estos fondos quedarán sujetos a un período de bloqueo irrevocable de tres años, durante los cuales no será posible hacer uso de los mismos.

\section{Operación y construcción del ecosistema}

Además de lo previsto en el inciso anterior, los fondos NAS remanentes serán utilizados para la expansión operativa y la construcción del ecosistema de Nebulas, que incluye pero no se limita a:

\begin{itemize}
\item la incubación del ecosistema de DApps
\item la comunidad de desarrolladores
\item la colaboración empresarial
\item el marketing
\item el estudio académico
\item la educación
\item la ley y la regulación
\item las inversiones institucionales.
\end{itemize}

\chapter*{INFORMACIÓN IMPORTANTE PARA POTENCIALES CONTRIBUYENTES}

ESTE LIBRO BLANCO NO CONSTITUYE UN PROSPECTO NI UN DOCUMENTO DE OFERTA Y NO CONSTITUYE NI PRETENDE CONSTITUIR UNA OFERTA DE VENTA, NI LA SOLICITUD DE UNA OFERTA DE COMPRA, UNA INVERSIÓN, UN VALOR O UN PRODUCTO BÁSICO, NI UNA OPCIÓN NI NINGÚN OTRO DERECHO PARA ADQUIRIR DICHA INVERSIÓN, VALOR O PRODUCTO BÁSICO. ESTE LIBRO BLANCO NO HA SIDO REVISADO, TRANSMITIDO NI PRESENTADO A NINGUNA AGENCIA FEDERAL O ESTATAL DE LOS ESTADOS UNIDOS NI A NINGUNA OTRA AGENCIA U ORGANIZACIÓN DE AUTORREGULACIÓN EXTRANJERA. ESTE LIBRO BLANCO NO CONSTITUYE UN CONSEJO PARA LA COMPRA DE CUALQUIER CRIPTODIVISA NI DEBE SER UTILIZADO EN RELACIÓN CON CUALQUIER CONTRATO O DECISIÓN DE CONTRIBUCIÓN.

ESTE LIBRO BLANCO CONTIENE DECLARACIONES A FUTURO QUE SE BASAN EN LAS CREENCIAS DE NEBULAS, ASÍ COMO CIERTAS SUPOSICIONES HECHAS EN BASE A LA INFORMACIÓN DISPONIBLE PARA ELLOS. EL PROYECTO, TAL COMO SE PREVÉ EN ESTE DOCUMENTO, ESTÁ EN FASE DE DESARROLLO Y SE ACTUALIZA CONSTANTEMENTE, INCLUYENDO, ENTRE OTRAS COSAS, LAS CARACTERÍSTICAS TÉCNICAS Y DE GOBERNANZA CLAVE. EN CONSECUENCIA, SI EL PROYECTO SE COMPLETA, PUEDE DIFERIR SIGNIFICATIVAMENTE DE LO EXPUESTO AQUÍ. NO SE DA NINGUNA REPRESENTACIÓN O GARANTÍA EN CUANTO AL LOGRO O RAZONABILIDAD DE CUALQUIER PLAN, PROYECCIONES FUTURAS O PROSPECTOS Y NADA EN ESTE INFORME TÉCNICO ES O DEBE SER TOMADO COMO UNA PROMESA O DESCRIPCIÓN EN CUANTO AL FUTURO.

LAS CRIPTODIVISAS NAS NO SON VALORES NI MATERIAS PRIMAS, Y NO CONSTITUYEN UNA OPCIÓN O UN COMPROMISO A PLAZO PARA ADQUIRIR O RECIBIR VALORES O MATERIAS PRIMAS O INSTRUMENTOS FINANCIEROS DE NINGÚN TIPO.

ASIMISMO, LA CRIPTODIVISA NAS NO HA SIDO REGISTRADA BAJO LA LEY DE VALORES DE LOS ESTADOS UNIDOS DE 1933, NI BAJO SU ENMIENDA, NI BAJO LAS LEYES DE VALORES DE NINGÚN ESTADO DE LOS ESTADOS UNIDOS O LAS LEYES DE VALORES DE NINGUNA OTRA JURISDICCIÓN EXTRANJERA, NI SE CONTEMPLA ESE REGISTRO. LA ADQUISICIÓN Y ENTREGA DE NAS NO ESTÁ SUJETA A LAS PROTECCIONES DE NINGUNA LEY QUE REGULE LOS VALORES, MATERIAS PRIMAS U OTROS TIPOS DE INSTRUMENTOS FINANCIEROS, INCLUYENDO, SIN LIMITACIÓN, LAS PROTECCIONES PROPORCIONADAS BAJO LA LEY DE VALORES O LEYES ESTATALES COMPARABLES DE LOS ESTADOS UNIDOS O LAS LEYES COMPARABLES DE CUALQUIER JURISDICCIÓN EXTRANJERA.

LA POSESIÓN DE NAS NO CONLLEVA NINGÚN DERECHO, YA SEA EXPRESO O IMPLÍCITO, SALVO UN DERECHO O EXPECTATIVA POTENCIAL LIMITADOS EN EL FUTURO PARA UTILIZAR NAS TAL Y COMO SE ESTABLECE EN ESTE LIBRO BLANCO Y EN LOS TÉRMINOS Y CONDICIONES DE DISTRIBUCIÓN. NAS SE PONDRÁ A DISPOSICIÓN DE LOS CONTRIBUYENTES ÚNICAMENTE PARA PROPORCIONAR O RECIBIR SERVICIOS EN LA PLATAFORMA DE NEBULAS Y PARA APOYAR EL DESARROLLO, PRUEBA, DESPLIEGUE Y OPERACIÓN DE LA PLATAFORMA DE NEBULAS. NAS ESTÁ DESTINADO A FINES DE INVERSIÓN, ESPECULATIVOS U OTROS FINES FINANCIEROS. NAS NAS NO REPRESENTA NI CONSTITUYE:

\begin{itemize}
\item DERECHO DE PROPIEDAD O PARTICIPACIÓN, ACCIÓN, CAPITAL, VALOR, MATERIA PRIMA, BONO, INSTRUMENTO DE DEUDA O CUALQUIER OTRO INSTRUMENTO FINANCIERO O INVERSIÓN CON DERECHOS EQUIVALENTES;
\item DERECHO A RECIBIR INGRESOS FUTUROS, GANANCIAS, DIVIDENDOS, INTERESES, ACCIONES, ACCIONES, VALORES O CUALQUIER OTRA FORMA DE PARTICIPACIÓN, ECONÓMICA O DE OTRO TIPO, O CUALQUIER DERECHO DE GOBIERNO EN O EN RELACIÓN CON NAS, LA PLATAFORMA DE NEBULAS O CUALQUIERA DE LOS SOCIOS DE LA COALICIÓN DE NEBULAS;
\item CUALQUIER FORMA DE DINERO O MONEDA DE CURSO LEGAL EN CUALQUIER JURISDICCIÓN NI CONSTITUYE NINGUNA REPRESENTACIÓN DEL DINERO (INCLUIDO EL DINERO ELECTRÓNICO);
\item EL SUMINISTRO DE CUALQUIER BIEN O SERVICIO ANTES DE LA FECHA EN QUE NAS PUEDA SER ENTREGADO A LOS CONTRIBUYENTES;
\item CUALQUIER DERECHO FUTURO DE VENDER NAS, O COMERCIAR NAS A O CON CUALQUIER OTRA PARTE.
\end{itemize}

LA CRIPTODIVISA NAS SERÁ DISTRIBUIDA POR DISTRIBUTED ASSET LTD., DE ACUERDO CON LOS TÉRMINOS Y CONDICIONES ESTABLECIDOS EN LOS TÉRMINOS Y CONDICIONES DE DISTRIBUCIÓN. ANTES DE ADQUIRIR NAS, DISTRIBUTED ASSET LTD., PROPORCIONARÁ UNA COPIA DE LOS TÉRMINOS Y CONDICIONES DE DISTRIBUCIÓN A LOS POSIBLES CONTRIBUYENTES. LOS CONTRIBUYENTES DEBEN RECONOCER, ACEPTAR Y ESTAR DE ACUERDO CON LOS TÉRMINOS Y CONDICIONES DE DISTRIBUCIÓN PARA PODER ADQUIRIR LOS PRODUCTOS. DISTRIBUTED ASSET LTD. PUEDE DECIDIR, A SU ENTERA DISCRECIÓN, MODIFICAR LOS TÉRMINOS Y CONDICIONES DE DISTRIBUCIÓN, O ABANDONAR LA PLATAFORMA DE NEBULAS Y RENUNCIAR A EMITIR CUALQUIER NOTIFICACIÓN ADICIONAL.

NEBULAS BUSCA OPERAR EN PLENO CUMPLIMIENTO DE LAS LEYES Y REGLAMENTOS APLICABLES Y OBTENER LAS LICENCIAS Y APROBACIONES NECESARIAS, SEGÚN SU OPINIÓN, EN LOS MERCADOS CLAVE. ESTO SIGNIFICA QUE EL DESARROLLO Y DESPLIEGUE DE TODAS LAS CARACTERÍSTICAS DE LA PLATAFORMA DE NEBULAS, TAL Y COMO SE DESCRIBE EN ESTE DOCUMENTO, NO ESTÁN GARANTIZADOS. ES PROBABLE QUE SE REQUIERAN LICENCIAS Y/O APROBACIONES REGULATORIAS EN UNA SERIE DE JURISDICCIONES PERTINENTES EN LAS QUE PUEDAN TENER LUGAR LAS ACTIVIDADES DESCRITAS. NO ES POSIBLE GARANTIZAR, Y NADIE LO GARANTIZA, QUE DICHAS LICENCIAS O APROBACIONES SE OBTENDRÁN EN UN PLAZO DETERMINADO O EN ABSOLUTO. ESTO SIGNIFICA QUE NEBULAS Y OTRAS CARACTERÍSTICAS DE SU PLATAFORMA PUEDEN NO ESTAR DISPONIBLES EN CIERTOS MERCADOS, O NO ESTAR DISPONIBLES EN NINGÚN MERCADO EN ABSOLUTO. ESTO PODRÍA REQUERIR LA REESTRUCTURACIÓN DE ESE ECOSISTEMA Y/O SU INDISPONIBILIDAD EN TODOS O EN ALGUNOS ASPECTOS.

NEBULAS SE RESERVA EL DERECHO DE REVISAR Y ALTERAR ESTE DOCUMENTO A SU ENTERA DISCRECIÓN. CUALQUIER REVISIÓN DE ESTE LIBRO BLANCO ESTARÁ DISPONIBLE EN EL SITIO WEB DE NEBULAS EN: \url{NEBULAS.IO}.

\end{document}