## Libro blanco no-técnico

Traducción del [non-technical whitepaper](https://github.com/nebulasio/nebdocs/blob/master/docs/go-nebulas/papers/nebulas-white-paper.md) de [Nebulas](https://nebulas.io).

Consúltese [esta página](https://go.nebulas.io/project/96) para más información acerca del proyecto de traducción y su recompensa asociada.